from django.urls import path
from .reportes import imprimir_devolucion_total
from .views import DevolucionNew, DevolucionView



urlpatterns = [
    path('devolucion', DevolucionView.as_view(), name='devolucion_list'),
    path('devolucion/new', DevolucionNew.as_view(), name='devolucion_new'),
    path('devolucion/reporte', imprimir_devolucion_total, name='devolucion_reporte'),
    
    
   
]