from django import forms
from .models import Devolucion
from prestamos1.models import Prestamo


class DevolucionForm(forms.ModelForm):
    # mostrar solo las categorias activas o True
    prestamo = forms.ModelChoiceField(
        queryset = Prestamo.objects.filter(estado=True)
        .order_by('id')
    )
    
    class Meta:
        model = Devolucion
        fields = ['prestamo', 'observacion']
        exclude = ['devolucion_id']
        labels = {'prestamo': 'Prestamo', 'observacion': 'Observacion'}
        widget = {'observacion': forms.TextInput}

    # sobreeescribir el init para poder agregar clases de botstrap a los inputs
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # iteramos todos los campos
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

        self.fields['prestamo'].empty_label = 'Seleccione Prestamo'
        