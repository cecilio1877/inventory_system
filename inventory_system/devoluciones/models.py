from django.db import models
from prestamos1.models import Prestamo
from productos.models import Producto
from user.models import ClaseModelo
import uuid
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

# Create your models here.


''' devolucion model '''
class Devolucion(ClaseModelo):
    devolucion_id = models.CharField(max_length=100, null=False, blank=False, unique=True)
    fecha_devolucion = models.DateTimeField(auto_now_add=True)
    prestamo = models.ForeignKey(Prestamo, on_delete=models.CASCADE)
    observacion = models.CharField(max_length=100)


    def __str__(self):
        return '{}'.format(self.devolucion_id)

    class Meta:
        verbose_name_plural = 'Devoluciones'


def set_devolucion_id(sender, instance, *args, **kwargs):
    if not instance.devolucion_id:
        instance.devolucion_id = str(uuid.uuid4())[:8]

pre_save.connect(set_devolucion_id, sender=Devolucion)


@receiver(post_save, sender=Devolucion)
def actualizar_prestamo(sender, instance, **kwargs):
    
    id_prestamo = instance.prestamo.id
    id_producto = instance.prestamo.producto.id

    pre = Prestamo.objects.filter(pk=id_prestamo).first()
    pro = Producto.objects.filter(pk=id_producto).first()
    if pre:
        pre.estado=False
        pre.save()

    if pro:
        pro.estado_prestamo=True
        pro.save()

