from django.contrib import admin
from .models import Devolucion
# Register your models here.

class DevolucionAdmin(admin.ModelAdmin):
    fields = ('uc', 'prestamo', 'observacion')
    list_display = ('__str__','fecha_devolucion', 'observacion')


admin.site.register(Devolucion, DevolucionAdmin)