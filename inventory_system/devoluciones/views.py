from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from .models import Devolucion
from .forms import DevolucionForm
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin


class DevolucionView(LoginRequiredMixin, generic.ListView):
    model = Devolucion
    template_name = 'devoluciones/devolucion_list.html'
    context_object_name = 'obj'
    login_url = 'bases:login'


class DevolucionNew(SuccessMessageMixin, LoginRequiredMixin, generic.CreateView):
    model = Devolucion
    template_name = 'devoluciones/devolucion_form.html'
    context_object_name = 'obj'
    form_class = DevolucionForm
    success_url = reverse_lazy('devoluciones:devolucion_list')  
    login_url = 'bases:login'
    success_message = 'Registro de Devolucion Creado con Exito'

    # para tomar el id del user y se guarda en el campo uc
    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)


