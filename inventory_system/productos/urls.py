from django.urls import path
from .views import CategoriaEdit, CategoriaNew, CategoriaView, categoria_inactivar, MarcaEdit, MarcaNew, MarcaView, marca_inactivar,  ProductoView, ProductoNew, ProductoEdit, producto_inactivar
from .reportes import imprimir_producto_total
#from .views import 

urlpatterns = [
    path('categoria', CategoriaView.as_view(), name='categoria_list'),
    path('categoria/new', CategoriaNew.as_view(), name='categoria_new'),
    path('categoria/edit/<int:pk>', CategoriaEdit.as_view(), name='categoria_edit'),
    path('categoria/inactivar/<int:id>', categoria_inactivar, name='categoria_inactivar'),

    path('marca', MarcaView.as_view(), name='marca_list'),
    path('marca/new', MarcaNew.as_view(), name='marca_new'),
    path('marca/edit/<int:pk>', MarcaEdit.as_view(), name='marca_edit'),
    path('marca/inactivar/<int:id>', marca_inactivar, name='marca_inactivar'),


    path('productos', ProductoView.as_view(), name='producto_list'),
    path('productos/new', ProductoNew.as_view(), name='producto_new'),
    path('productos/edit/<int:pk>', ProductoEdit.as_view(), name='producto_edit'),
    path('productos/inactivar/<int:id>', producto_inactivar, name='producto_inactivar'),

    path('productos/reporte', imprimir_producto_total, name='producto_reporte'),

]