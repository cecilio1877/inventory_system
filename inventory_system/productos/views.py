from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from .models import Categoria, Producto, Marca
from .forms import CategoriaForm, ProductoForm, MarcaForm
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
# Create your views here.


class CategoriaView(LoginRequiredMixin, generic.ListView ):
    model = Categoria
    template_name = 'productos/categoria_list.html'
    context_object_name = 'obj'
    login_url = 'bases:login'

# este CreateView dice que es un form que ingresa datos
class CategoriaNew(SuccessMessageMixin, LoginRequiredMixin, generic.CreateView):
    model = Categoria
    template_name = 'productos/categoria_form.html'
    context_object_name = 'obj'
    form_class = CategoriaForm
    success_url = reverse_lazy('productos:categoria_list')  
    login_url = 'bases:login'
    success_message = 'Categoria Creado con Exito'

    # para tomar el id del user y se guarda en el campo uc
    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)


class CategoriaEdit(SuccessMessageMixin, LoginRequiredMixin, generic.UpdateView):
    model = Categoria
    template_name = 'productos/categoria_form.html'
    context_object_name = 'obj'
    form_class = CategoriaForm
    success_url = reverse_lazy('productos:categoria_list')  
    login_url = 'bases:login'
    success_message = 'Categoria Modificada'


def categoria_inactivar(request, id):
    # consulta a la bd para obtener el id
    cat = Categoria.objects.filter(pk=id).first()
    contexto = {}
    template_name = 'productos/producto_inactivar.html'
    
    if not cat:
        return redirect('productos:categoria_list')
    
    if request.method =='GET':
        contexto = {'obj':cat}
    
    # cambio el estado de la marca y lo guardo en la bd
    if request.method =='POST':
        cat.estado=False
        cat.save()
        messages.success(request, 'Categoria INACTIVADA')
        return redirect('productos:categoria_list')
    return render(request, template_name, contexto)


""" Marca Views """

class MarcaView(LoginRequiredMixin, generic.ListView ):
    model = Marca
    template_name = 'productos/marca_list.html'
    context_object_name = 'obj'
    login_url = 'bases:login'

# este CreateView dice que es un form que ingresa datos
class MarcaNew(SuccessMessageMixin, LoginRequiredMixin, generic.CreateView):
    model = Marca
    template_name = 'productos/marca_form.html'
    context_object_name = 'obj'
    form_class = MarcaForm
    success_url = reverse_lazy('productos:marca_list')  
    login_url = 'bases:login'
    success_message = 'Marca Creada con exito'

    # para tomar el id del user y se guarda en el campo uc
    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)


class MarcaEdit(SuccessMessageMixin, LoginRequiredMixin, generic.UpdateView):
    model = Marca
    template_name = 'productos/marca_form.html'
    context_object_name = 'obj'
    form_class = MarcaForm
    success_url = reverse_lazy('productos:marca_list')  
    login_url = 'bases:login'
    success_message = 'Marca Modificada'


def marca_inactivar(request, id):
    # consulta a la bd para obtener el id
    marca = Marca.objects.filter(pk=id).first()
    contexto = {}
    template_name = 'productos/producto_inactivar.html'
    
    if not marca:
        return redirect('productos:marca_list')
    
    if request.method =='GET':
        contexto = {'obj':marca}
    
    # cambio el estado de la marca y lo guardo en la bd
    if request.method =='POST':
        marca.estado=False
        marca.save()
        messages.success(request, 'Marca INACTIVADA')
        return redirect('productos:marca_list')
    return render(request, template_name, contexto)


"""
Producto Views
"""
class ProductoView(LoginRequiredMixin, generic.ListView):
    model = Producto
    template_name = 'productos/producto_list.html'
    context_object_name = 'obj'
    login_url = 'bases:login'

class ProductoNew(SuccessMessageMixin, LoginRequiredMixin, generic.CreateView):
    model = Producto
    template_name = 'productos/producto_form.html'
    context_object_name = 'obj'
    form_class = ProductoForm
    success_url = reverse_lazy('productos:producto_list')  
    login_url = 'bases:login'
    success_message = 'Prducto Creado con exito'

    # para tomar el id del user y se guarda en el campo uc
    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)

class ProductoEdit(SuccessMessageMixin, LoginRequiredMixin, generic.UpdateView):
    model = Producto
    template_name = 'productos/producto_form.html'
    context_object_name = 'obj'
    form_class = ProductoForm
    success_url = reverse_lazy('productos:producto_list')  
    login_url = 'bases:login'
    success_message = 'Producto Modificado'


def producto_inactivar(request, id):
    # consulta a la bd para obtener el id
    pro = Producto.objects.filter(pk=id).first()
    contexto = {}
    template_name = 'productos/producto_inactivar.html'
    
    if not pro:
        return redirect('productos:producto_list')
    
    if request.method =='GET':
        contexto = {'obj':pro}
    
    # cambio el estado de la marca y lo guardo en la bd
    if request.method =='POST':
        pro.estado=False
        pro.save()
        messages.success(request, 'Producto INACTIVADO')
        return redirect('productos:producto_list')
    return render(request, template_name, contexto)