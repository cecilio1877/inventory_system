from django import forms

from .models import Categoria, Producto, Marca


class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ['descripcion', 'estado']
        labels = {'descripcion': 'Descripcion de la Categoria', 'estado': 'Estado'}
        widget = {'descripcion': forms.TextInput}

    # sobreeescribir el init para poder agregar clases de botstrap a los inputs
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # iteramos todos los campos
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })


""" marca form """
class MarcaForm(forms.ModelForm):
    class Meta:
        model = Marca
        fields = ['descripcion', 'estado']
        labels = {'descripcion': 'Descripcion de la Marca', 'estado': 'Estado'}
        widget = {'descripcion': forms.TextInput}

    # sobreeescribir el init para poder agregar clases de botstrap a los inputs
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # iteramos todos los campos
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

""" producto form """
class ProductoForm(forms.ModelForm):

    categoria = forms.ModelChoiceField(
        queryset = Categoria.objects.filter(estado=True)
        .order_by('descripcion')

    )
    marca = forms.ModelChoiceField(
        queryset = Marca.objects.filter(estado=True)
        .order_by('descripcion')

    )
    class Meta:
        model = Producto
        fields = ['descripcion', 'marca', 'modelo', 'estado', 'categoria', 'imagen']
        labels = {'descripcion':'Descripcion Producto', 'marca':'Marca', 'modelo':'Modelo', 'estado': 'Estado', 'categoria': 'Categoria'}
        widget = {'descripcion': forms.TextInput, 'modelo': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # iteramos todos los campos
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control p-2'
            })
        

        self.fields['categoria'].empty_label = 'Seleccione categoria'
        self.fields['marca'].empty_label = 'Seleccione marca'