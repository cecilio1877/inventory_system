from django.db import models
from user.models import ClaseModelo

# Create your models here.

class Categoria(ClaseModelo):
    descripcion = models.CharField(max_length=50, unique=True)


    # sobreecribo para tomar el valor de descripcion
    def __str__(self):
        return '{}'.format(self.descripcion)

    # sobreescribo este metodo para guardar en mayusculas la descripcion
    def save(self):
        self.descripcion = self.descripcion.upper()
        super(Categoria, self).save()

class Marca(ClaseModelo):
    descripcion = models.CharField(max_length=50, unique=True)


    # sobreecribo para tomar el valor de descripcion
    def __str__(self):
        return '{}'.format(self.descripcion)

    # sobreescribo este metodo para guardar en mayusculas la descripcion
    def save(self):
        self.descripcion = self.descripcion.upper()
        super(Marca, self).save()

class Producto(ClaseModelo):
    descripcion = models.CharField(max_length=50)
    modelo = models.CharField(max_length=50)
    estado_prestamo = models.BooleanField(default=True)
    
    categoria = models.ForeignKey(Categoria, null=True, blank=True, on_delete=models.CASCADE)
    marca = models.ForeignKey(Marca, null=True, blank=True, on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to='producto/', null=True, blank=True)

    # sobreecribo para tomar el valor de descripcion
    def __str__(self):
        return '{}:{}'.format(self.categoria.descripcion, self.marca.descripcion) 

    # sobreescribo este metodo para guardar en mayusculas la descripcion
    def save(self):
        self.descripcion = self.descripcion.upper()
        self.modelo = self.modelo.upper()
        super(Producto, self).save()