from django.db import models
from user.models import ClaseModelo

# Create your models here.


class Trabajador(ClaseModelo):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    cedula = models.CharField(max_length=50)
    direccion = models.TextField()
    correo = models.EmailField()
    celular = models.CharField(max_length=10)
    imagen = models.ImageField(upload_to='trabajador/', null=True, blank=True)


    def __str__(self):
        return '{}-{}'.format(self.nombre, self.apellido)


    def save(self):
        self.nombre = self.nombre.upper()
        self.apellido = self.apellido.upper()
        super(Trabajador, self).save()

    class Meta:
        verbose_name_plural = 'Trabajadores'
