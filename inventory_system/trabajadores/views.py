from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from .models import Trabajador
from .forms import TrabajadorForm
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages

# Create your views here.

class TrabajadoView(LoginRequiredMixin, generic.ListView):
    model = Trabajador
    template_name = 'trabajadores/trabajador_list.html'
    context_object_name = 'obj'
    login_url = 'user:login'
 

class TrabajadorNew(SuccessMessageMixin, LoginRequiredMixin, generic.CreateView):
    model = Trabajador
    template_name = 'trabajadores/trabajador_form.html'
    context_object_name = 'obj'
    form_class = TrabajadorForm
    success_url = reverse_lazy('trabajadores:trabajador_list')
    login_url = 'user:login'
    success_message = 'Trabajador Creado con Exito'

    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)


# edit Trabajador 
class TrabajadorEdit(SuccessMessageMixin, LoginRequiredMixin, generic.UpdateView):
    model = Trabajador
    template_name = 'trabajadores/trabajador_form.html'
    context_object_name = 'obj'
    form_class = TrabajadorForm
    success_url = reverse_lazy('trabajadores:trabajador_list')  # cuando se manda el form hay que poner la ruta de donde va despues no puede quedar en la misma ruta
    login_url = 'user:login'
    success_message = 'Trabajador actualizado'


def trabajador_inactivar(request, id):
    # consulta a la bd para obtener el id
    tra = Trabajador.objects.filter(pk=id).first()
    contexto = {}
    template_name = 'trabajadores/trabajador_del.html'
    
    if not tra:
        return redirect('trabajadores:trabajador_list')
    
    if request.method =='GET':
        contexto = {'obj':tra}
    
    # cambio el estado de la marca y lo guardo en la bd
    if request.method =='POST':
        tra.estado=False
        tra.save()
        messages.success(request, 'Trabajador INACTIVADO')
        return redirect('trabajadores:trabajador_list')
    return render(request, template_name, contexto)
