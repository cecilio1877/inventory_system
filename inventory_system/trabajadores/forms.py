from django import forms
from .models import Trabajador


class TrabajadorForm(forms.ModelForm):
    class Meta:
        model = Trabajador
        fields = ['nombre', 'apellido', 'cedula', 'direccion', 'correo', 'celular','estado', 'imagen']
        labels = {'nombre': 'Nombre', 'apellido': 'Apellido', 'cedula': 'Cedula', 'direccion': 'Direccion', 'correo': 'Correo', 'celular': 'Celular', 'estado': 'Estado'}

        widget = {'nombre': forms.TextInput, 'apellido': forms.TextInput, 'cedula': forms.NumberInput, 'direccion': forms.TextInput, 'correo': forms.EmailInput, 'celular': forms.NumberInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # iteramos todos los campos
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                    'class': 'form-control'
            })