from django.urls import path
from .views import TrabajadoView, TrabajadorNew, TrabajadorEdit, trabajador_inactivar
from .reportes import imprimir_trabajador_total
urlpatterns = [
    path('', TrabajadoView.as_view(), name='trabajador_list'),
    path('new', TrabajadorNew.as_view(), name='trabajador_new'),
    path('edit/<int:pk>', TrabajadorEdit.as_view(), name='trabajador_edit'),
    path('inactivar/<int:id>', trabajador_inactivar, name='trabajador_delete'),

    path('trabajadores/reporte', imprimir_trabajador_total, name='trabajador_reporte'),
]