from django.urls import path
from .reportes import imprimir_prestamo, imprimir_prestamo_total
from .views import PrestamoView, PrestamoNew



urlpatterns = [
    path('prestamo', PrestamoView.as_view(), name='prestamo_list'),
    path('prestamo/new', PrestamoNew.as_view(), name='prestamo_new'),

    path('prestamo/<int:prestamo_id>/imprimir', imprimir_prestamo, name='prestamo_imprimir_uno'),
    path('prestamo/reporte', imprimir_prestamo_total, name='prestamo_reporte'),
   
]