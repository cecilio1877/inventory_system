from django.contrib import admin
from .models import Prestamo
# Register your models here.


class PrestamoAdmin(admin.ModelAdmin):
    fields = ('uc', 'trabajador', 'producto', 'observacion')
    list_display = ('__str__', 'prestamo_id','fechaPrestamo')


admin.site.register(Prestamo, PrestamoAdmin)
