import os
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.contrib.staticfiles import finders
from .models import Prestamo
from django.utils import timezone



def link_callback(uri, rel):
            """
            Convert HTML URIs to absolute system paths so xhtml2pdf can access those
            resources
            """
            result = finders.find(uri)
            if result:
                    if not isinstance(result, (list, tuple)):
                            result = [result]
                    result = list(os.path.realpath(path) for path in result)
                    path=result[0]
            else:
                    sUrl = settings.STATIC_URL        # Typically /static/
                    sRoot = settings.STATIC_ROOT      # Typically /home/userX/project_static/
                    mUrl = settings.MEDIA_URL         # Typically /media/
                    mRoot = settings.MEDIA_ROOT       # Typically /home/userX/project_static/media/

                    if uri.startswith(mUrl):
                            path = os.path.join(mRoot, uri.replace(mUrl, ""))
                    elif uri.startswith(sUrl):
                            path = os.path.join(sRoot, uri.replace(sUrl, ""))
                    else:
                            return uri

            # make sure that file exists
            if not os.path.isfile(path):
                    raise Exception(
                            'media URI must start with %s or %s' % (sUrl, mUrl)
                    )
            return path


# vista de un solo prestamo
def imprimir_prestamo(request, prestamo_id):
    template_path = 'prestamos/prestamo_uno.html'
    today = timezone.now()

    pre = Prestamo.objects.filter(id=prestamo_id).first()
    
    context ={
        "prestamo": pre,
        "today": today,
        "request": request
    }

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="ficha_prestamo.pdf"'
    template = get_template(template_path)
    html = template.render(context)

    pisa_status = pisa.CreatePDF(
            html, dest=response, link_callback=link_callback)

    if pisa_status.err:
            return HttpResponse('Ocurrio un error <pre>' + html + '</pre>')
    return response


def imprimir_prestamo_total(request):
    template_path = 'prestamos/prestamo_all.html'
    today = timezone.now()

    pre = Prestamo.objects.all()
    
    context ={
        "obj": pre,
        "today": today,
        "request": request
    }

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="reporte_prestamo.pdf"'
    template = get_template(template_path)
    html = template.render(context)

    pisa_status = pisa.CreatePDF(
            html, dest=response, link_callback=link_callback)

    if pisa_status.err:
            return HttpResponse('Ocurrio un error <pre>' + html + '</pre>')
    return response