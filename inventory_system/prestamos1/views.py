from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from .models import Prestamo
from .forms import PrestamoForm
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin


class PrestamoView(LoginRequiredMixin, generic.ListView):
    model = Prestamo
    template_name = 'prestamos/prestamo_list.html'
    context_object_name = 'obj'
    login_url = 'bases:login'


class PrestamoNew(SuccessMessageMixin, LoginRequiredMixin, generic.CreateView):
    model = Prestamo
    template_name = 'prestamos/prestamo_form.html'
    context_object_name = 'obj'
    form_class = PrestamoForm
    success_url = reverse_lazy('prestamos:prestamo_list')  
    login_url = 'bases:login'
    success_message = 'Registro de Prestamo Creado con Exito'

    # para tomar el id del user y se guarda en el campo uc
    def form_valid(self, form):
        form.instance.uc = self.request.user
        return super().form_valid(form)


