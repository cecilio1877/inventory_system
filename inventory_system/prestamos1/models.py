from django.db import models
from trabajadores.models import Trabajador
from productos.models import Producto
from user.models import ClaseModelo
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.text import slugify
import uuid
# Create your models here. 



class Prestamo(ClaseModelo):
    prestamo_id = models.CharField(max_length=100, null=False, blank=False, unique=True)
    fechaPrestamo = models.DateTimeField(auto_now_add=True)
    trabajador = models.ForeignKey(Trabajador, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    ''' slug = models.SlugField(null=False, blank=False, unique=True) '''
    observacion = models.CharField(max_length=100)

    def __str__(self):
        return '{}:{}:{}'.format(self.trabajador,self.producto,self.prestamo_id)

    class Meta:
        verbose_name_plural = 'Prestamos'

def set_prestamo_id(sender, instance, *args, **kwargs):
    if not instance.prestamo_id:
        instance.prestamo_id = str(uuid.uuid4())[:8]

pre_save.connect(set_prestamo_id, sender=Prestamo)


@receiver(post_save, sender=Prestamo)
def actualizar_producto(sender, instance, **kwargs):
    
    id_producto = instance.producto.id
    

    pro = Producto.objects.filter(pk=id_producto).first()

    if pro:
        pro.estado_prestamo=False
        pro.save()
