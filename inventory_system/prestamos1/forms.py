from django import forms
from .models import Prestamo
from productos.models import Producto
from trabajadores.models import Trabajador

class PrestamoForm(forms.ModelForm):
    # mostrar solo las categorias activas o True
    trabajador = forms.ModelChoiceField(
        queryset = Trabajador.objects.filter(estado=True)
        .order_by('id')

    )
    producto = forms.ModelChoiceField(
        queryset = Producto.objects.filter(estado=True, estado_prestamo=True)
        .order_by('id')

    )
    class Meta:
        model = Prestamo
        fields = ['trabajador','producto', 'observacion']
        exclude = ['prestamo_id']
        labels = {'trabajador': 'Trabajador', 'producto': 'Producto', 'observacion': 'Observacion'}
        widget = {'observacion': forms.TextInput}

    # sobreeescribir el init para poder agregar clases de botstrap a los inputs
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # iteramos todos los campos
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

        self.fields['trabajador'].empty_label = 'Seleccione Empleado'
        self.fields['producto'].empty_label = 'Seleccione Producto'